#include <cstdio>
#include <vector>

using namespace std;

const int maxn=50010;

int N;
vector<int> pre, in;

// pre[root] in[start -  end]
int post(int root, int start, int end)
{
	if(start>=end) return in[start];
	int i;
	for(i=start;i<end;i++)
	{
		if(in[i]==pre[root]) break;
	}
	if(i==start)  // 左子树为空
	{
		return post(root+1,i+1,end);
	}
	else
	{
		return post(root+1 ,start, i-1);
	}
}

int main(void)
{
	//freopen("f:/tmp/C2.txt","r",stdin);
	scanf("%d",&N);
	int tmp;
	for(int i=0;i<N;i++)
	{
		scanf("%d",&tmp);
		pre.push_back(tmp);
	}
	for(int i=0;i<N;i++)
	{
		scanf("%d",&tmp);
		in.push_back(tmp);
	}
	
	int res = post(0, 0, N-1);
	printf("%d",res);
	return 0;
}
