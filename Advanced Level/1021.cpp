//1021 连通性判断，连通分量个数 
#include <cstdio>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;
const int maxn=10010; 

int N, maxDeepth=0;
vector<vector<int> > g;
set<int> s;
bool vis[maxn];
vector<int> maxDeepthNodes;

void dfs(int root, int deepth)
{
	vis[root]=true;
	if(maxDeepth < deepth)
	{
		maxDeepth = deepth;
		maxDeepthNodes.clear();
		maxDeepthNodes.push_back(root);
	}
	else if(maxDeepth == deepth)
	{
		maxDeepthNodes.push_back(root);
	}
	
	for(int i=0;i<g[root].size();i++)
		if(!vis[g[root][i]])
			dfs(g[root][i], deepth+1);
}

int main(void)
{
	//freopen("f:/tmp/PAT/1.txt","r",stdin);
	scanf("%d",&N);
	g.resize(N+1);
	int a,b,cnt=0;
	for(int i=0;i<N-1;i++)
	{
		scanf("%d%d",&a,&b);
		g[a].push_back(b);
		g[b].push_back(a);
	}
	fill(vis,vis+N+1,false);
	int s1;
	for(int i=1;i<=N;i++)
	{
		if(vis[i]==false)
		{
			dfs(i,1);
			if(i==1)
			{
				for(int j=0;j<maxDeepthNodes.size();j++)
				{
				  s.insert(maxDeepthNodes[j]);
				  if(j==0)
				    s1 = maxDeepthNodes[j];
				}
			}
			cnt++;
		}
	} 
	if(cnt>=2)
	{
		printf("Error: %d components",cnt);
	}else if(cnt==1)
	{
		maxDeepth = 0;
	    maxDeepthNodes.clear();
	    fill(vis, vis+N+1, false);
	    dfs(s1,1);
	    for(int j=0;j<maxDeepthNodes.size();j++)
	      s.insert(maxDeepthNodes[j]);
	    for(auto x:s)
      	printf("%d\n",x);
	}
	
	return 0;
}