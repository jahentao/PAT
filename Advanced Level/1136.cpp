#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn=1010;

int N;

typedef struct node{
	int data[maxn];
	int length;
}BigNum;

void outputBigNum(BigNum &a)
{
	for(int i=0;i<a.length;i++)
	{
		printf("%d",a.data[i]);
	}
}

// add result restore in a
void bigNumAdd(BigNum &a, BigNum &b)
{
	int jinwei=0;
	int data[maxn], len=0;
	for(int i=a.length-1;i>=0;i--)
	{
		if(a.data[i]+b.data[i]+jinwei >=10)
		{
			data[len++] = (a.data[i]+b.data[i]+jinwei) % 10;
			jinwei=1;
		}else
		{
			data[len++] = (a.data[i]+b.data[i]+jinwei) % 10;
			jinwei=0;
		}
	}
	if(jinwei==1) data[len++]=1;
	a.length = len;
	for(int i=0;i<len;i++)
	{
		a.data[len-i-1] = data[i];
	}
}


void reverse(BigNum &a, BigNum &b)
{
	b.length = a.length;
	for(int i=0;i<a.length;i++)
	{
		b.data[b.length-i-1]=a.data[i];
	}
}

bool equal(BigNum &a, BigNum &b)
{
	bool flag = true;
	for(int i=0;i<a.length;i++)
	{
		if(a.data[i]!=b.data[i]){
			flag = false;
			break;
		}
	}
	return flag;
}

int main(void)
{
	//freopen("f:/tmp/A3.txt","r",stdin);
	string s;
	int cnt = 0;
	cin>>s;
	BigNum num, numr;
	num.length = s.length();
	numr.length = s.length();
	for(int i=0;i<num.length;i++)
	{
		num.data[i]=s[i]-'0';
		numr.data[numr.length-i-1]=num.data[i];
	}
	
	while(cnt<10)
	{
		//reverse
		reverse(num,numr);
		if(equal(num,numr))
		{
			outputBigNum(num);
			printf(" is a palindromic number.\n");
			break;
		}
		
		// add
		outputBigNum(num);
		printf(" + ");
		outputBigNum(numr);
		printf(" = ");
		bigNumAdd(num, numr);
		outputBigNum(num);
		printf("\n");
		
		cnt++;
	}
	if(cnt>=10)
	{
		printf("Not found in 10 iterations.");
	}
	return 0;
}
