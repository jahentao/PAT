#include <cstdio>
#include <map>
#include <algorithm>

using namespace std;

const int maxm=810;
const int maxn=610;

int image[maxm][maxn];
int M,N;
map<int, int> color;

bool cmp(pair<int,int> a, pair<int,int> b)
{
	return a.second > b.second;
}

int main(void)
{
	//freopen("F:/tmp/PAT/1.txt","r",stdin);
	scanf("%d%d",&M,&N);
	for(int i=0;i<M;i++)
	{
		for(int j=0;j<N;j++)
		{
			scanf("%d",&image[i][j]);
			color[image[i][j]]++;
		}
	}
	//把map中元素转存到vector中 
	vector<pair<int,int>> color_vec(color.begin(), color.end());
	sort(color_vec.begin(), color_vec.end(), cmp);
	printf("%d",color_vec[0].first);
	return 0;
}