#include <cstdio>
#include <vector>
using namespace std;

const int maxn = 31;

int N;
vector<int> in, post, levels;

/**
 * root为post后序遍历中的根的下标
 * start、end分别为中序遍历序列的起始点和终点
 * 递归的终止条件是 start>end
 * 需要识记的是递归的两段(root-1-end+i, start, i-1) 和 (root-1, i+1, end)
 * 需要注意：root是下标，在in先序中寻找是与post[root]比较的，而非root。
 */
void pre(int root, int start, int end, int index) {
    if(start > end) return ;
    int i = start;
    while(i < end && in[i] != post[root]) i++;
    levels[index]=post[root];
    pre(root - 1 - end + i, start, i - 1, 2*index+1); // L
    pre(root - 1, i + 1, end,2*index+2); // R
}

int main(void)
{
	//freopen("F:/tmp/PAT/1.txt","r",stdin);
	scanf("%d",&N);
	in.resize(N);
	post.resize(N);
	levels.resize(1000000, -1);
	for(int i=0;i<N;i++)
		scanf("%d",&post[i]);
	for(int i=0;i<N;i++)
		scanf("%d",&in[i]);
	pre(N-1,0,N-1,0);
	int cnt=0;
	for(int i=0;i<levels.size();i++)
	{
		if(levels[i]!=-1 && cnt!=N-1)
		{
		    printf("%d ",levels[i]);
		    cnt++;
		}
		else if(levels[i]!=-1)
		{
		    printf("%d",levels[i]);
		    break;
		}
	}
	return 0;
}