#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int N,K,P,maxFacSum=-1;
vector<int> TempSumVec, MaxSumVec, fac; 

void init() {
    int temp = 0, index = 1;
    while(temp <= N) {
        fac.push_back(temp);
        temp = pow(index++, P);
    }
}
void DFS(int index, int sum, int nowK, int facSum)
{
	if(sum==N && nowK==K)
	{
		if(facSum> maxFacSum)
		{
			MaxSumVec = TempSumVec;
			maxFacSum = facSum;
		}
		return;
	}
	if(sum>N || nowK>K) return;
	if(index-1>=0)
	{
		TempSumVec.push_back(index);
		DFS(index, sum+fac[index], nowK+1, facSum+index); // 选 
		TempSumVec.pop_back();
		DFS(index-1, sum, nowK, facSum); //不选 
	}
}

int main(void)
{
	//freopen("f:/tmp/PAT/1.txt","r",stdin);
	scanf("%d%d%d",&N,&K,&P);
	init();
	DFS(fac.size()-1,0,0,0);
	if(maxFacSum == -1) printf("Impossible\n");
	else
	{
		printf("%d = %d^%d",N, MaxSumVec[0],P);
		for(int i=1;i<MaxSumVec.size();i++)
		{
			printf(" + %d^%d",MaxSumVec[i],P);
		}
	}
	return 0;
}