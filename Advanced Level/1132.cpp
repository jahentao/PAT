/**
思路：通过sscanf的技巧将字符串转换为整数，其实也可以用s=s*10+c-'0'的典型的字符数组转整数的技巧。
该题仍需注意的测试点是，a、b不能为0，会有除0，浮点数错误。
*/
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;
const int maxn = 32;

int N;

int main(void)
{
  //freopen("f:/tmp/PAT/1.txt","r",stdin);
  scanf("%d",&N);
  char s[maxn];
  char sTmp[maxn];
  int num,a,b,j;
  for(int i=0;i<N;i++)
  {
    scanf("%s", &s);
    sscanf(s, "%d", &num);
    
    int K = strlen(s);
    for(j=0;j<K/2;j++) // char[] cpy
      sTmp[j]=s[j];
    sTmp[j]='\0';
    // a b
    sscanf(sTmp,"%d",&a);
    sscanf(s+K/2,"%d",&b);
    if(a && b)
      if(num % (a*b) == 0) printf("Yes\n");
      else printf("No\n");
    else printf("No\n");
  } 
  return 0;
}