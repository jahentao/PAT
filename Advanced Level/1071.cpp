#include <iostream>
#include <map>
#include <algorithm>

using namespace std;

const int maxn = 1048576;
map<string, int> words;

bool check(char c)
{
	return ('A'<=c&&c<='Z') || ('a'<=c&&c<='z') || ('0'<=c&&c<='9');
}

int main(void)
{
	//freopen("F:/tmp/PAT/1.txt","r",stdin);
	string s, word;
	getline(cin,s);
	int k=0;
	while(k<s.length())
	{
		word.clear();
		while(k<s.length() && check(s[k]))
		{
			if('A'<=s[k] && s[k]<='Z')
			{
				s[k]+=32;
			}
			word+=s[k++];
		}
		if(word!="")
		{
			words[word]++;
		}
		if(!check(s[k])) k++;
	}
	int Max=0;
	string res;
	for(map<string,int>::iterator it=words.begin();it!=words.end();it++)
	{
		if(it->second > Max)
		{
			res = it->first;
			Max = it->second;
		}
	}
	cout<<res<<" "<<Max<<endl;
	return 0;
}