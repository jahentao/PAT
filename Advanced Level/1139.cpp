#include <cstdio>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

const int maxn=310;

map<int, vector<int> > friends;
vector<pair<int,int> > result;
int g[10010][10010];

int N,M,K;

bool cmp(pair<int,int> pA, pair<int,int> pB)
{
  return pA.first == pB.first ? pA.second < pB.second : pA.first <= pB.first;
}
int main(void)
{
  //freopen("/home/jahentao/Documents/PAT/in/1139_1.txt","r",stdin);
  scanf("%d%d",&N,&M);
  int a,b;
  for(int i=0;i<M;i++)
  {
    scanf("%d%d",&a,&b);
    if(a*b<0) {
      if(a<0) a=-a;
      if(b<0) b=-b;
      
      g[a][b] = -1;
      g[b][a] = -1;
    }
    else{
      if(a<0) a=-a;
      if(b<0) b=-b;
      
      g[a][b] = 1;
      g[b][a] = 1;
      
      friends[a].push_back(b);
      friends[b].push_back(a);
    }
  }
  
  scanf("%d",&K);
  for(int i=0;i<K;i++)
  {
    scanf("%d%d",&a,&b);
    if(a<0) a=-a;
    if(b<0) b=-b;
    vector<int> va = friends[a];
    vector<int> vb = friends[b];
    result.clear();
    for(int j=0;j<va.size();j++)
    {
      for(int k=0;k<vb.size();k++)
      {
        if(g[va[j]][vb[k]]!=0 && va[j]!=b && vb[k]!=a){ // 特别是这里，同性朋友不包含对方！我与满分失之交臂
          result.push_back(make_pair(va[j],vb[k]));
        }
      }
    }
    printf("%d\n",result.size());
    //output
    sort(result.begin(),result.end(),cmp);
    for(int j=0;j<result.size();j++)
    {
      printf("%04d %04d\n",result[j].first,result[j].second);
    }
  }
  return 0;
}
