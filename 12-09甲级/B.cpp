#include <cstdio>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

const int maxn=10010;


int P,M,N;
map<string, vector<int> > stus;
vector<pair<string, vector<int> >> result;

bool cmp(pair<string, vector<int> > pA, pair<string, vector<int> > pB)
{
	return pA.second[3] == pB.second[3] ? pA.first < pB.first : pA.second[3] > pB.second[3];
}

int main(void)
{
	freopen("f:/tmp/B1.txt","r",stdin);
	scanf("%d%d%d",&P,&M,&N);
	int Gp, Gm, Gn;
	char name[21];
	for(int i=0;i<P;i++)
	{
		scanf("%s%d",name, &Gp);
		stus[name].resize(4,-1);
		stus[name][0]=Gp;
	}
	for(int i=0;i<M;i++)
	{
		scanf("%s%d",name, &Gm);
		stus[name].resize(4,-1);
		stus[name][1]=Gm;
	}
	for(int i=0;i<N;i++)
	{
		scanf("%s%d",name, &Gn);
		stus[name].resize(4,-1);
		stus[name][2]=Gn;
	}
	//calculate
	map<string, vector<int> >::iterator it;
	for(it=stus.begin();it!=stus.end();it++)
	{
		vector<int> v = it->second;
		float res =  v[1] > v[2] ? v[1]*0.4 + v[2]*0.6 : v[2];
		it->second[3] = (int)(res+0.5);
		if(v[0]>=200 && it->second[3]>=60)
		{
			result.push_back(make_pair(it->first,it->second));
		}
	}
	//sort
	sort(result.begin(),result.end(),cmp);
	//output
	for(int i=0;i<result.size();i++)
	{
		vector<int> v = result[i].second;
		printf("%s %d %d %d %d\n",result[i].first.c_str(), v[0], v[1], v[2], v[3]);
	}
	
	return 0;
}
